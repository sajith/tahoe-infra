This repository holds some Terraform files related to some of
Tahoe-LAFS project infrastructure.

Some things worth noting:

- The file ``tahoe-lafs.net.tf`` contains DNS updates for
  tahoe-lafs.net, which is more of domain name used for testing things
  out, and not really an official project domain.  At least not yet.

- The domain is registered with Gandi.net.  We use `Gandi Terraform
  provider`_.  Since this provider has not been "officially" published
  on HashiCorp's registy (see `related issue`_), we publish our copy
  there, and use that.

  (There are other copies of the Gandi Terraform provider in
  HashiCorp's registry that other people have published, but should we
  trust them?  I don't know.)

- Gandi API key is provided as an input (environment) variable, namely
  ``TF_VAR_GANDI_API_KEY_TAHOE_LAFS_NET``.

- The GitLab CI configuration is set up to run ``validate`` on new
  commits.  We do not (and likely should not) run ``plan`` and
  ``apply`` on CI, because that would require us to store Gandi API
  keys on GitLab.com.  Even though the keys are to be used as
  "protected" and "masked" environment variables in CI environment, we
  probably should not trust any third party with that.

.. _Gandi Terraform provider:  https://github.com/go-gandi/terraform-provider-gandi
.. _related issue: https://github.com/go-gandi/terraform-provider-gandi/issues/48
