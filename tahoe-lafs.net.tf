#
# This is for managing DNS records for tahoe-lafs.net.  The domain is
# with Gandi.net, and we will use a Terraform Gandi provider.
#
terraform {

  required_version = "~> 1.0.0"

  # https://github.com/go-gandi/terraform-provider-gandi is the Gandi
  # provider for Terraform, but it is not available in the Terraform
  # registry, and building that for CI is a little bit of a hassle.
  # We could publish a copy of the provider and use that for CI.
  # Other people have published their copies of the provider, but we
  # should probably be wary of them.
  #
  # To make own version of the provider, I cloned the GitHub
  # repository, made a release using goreleaser, and published the
  # release at https://registry.terraform.io/.
  required_providers {
    gandi = {
      version = "2.0.0-rc3b"
      source  = "sajith/gandi"
    }
  }
}

# Input env var should be TF_VAR_GANDI_API_KEY_TAHOE_LAFS_NET
variable "GANDI_API_KEY_TAHOE_LAFS_NET" {
  type        = string
  description = "Gandi.net API Key"
}

# # Input env var should be TF_VAR_GANDI_SHARING_ID_TAHOE_LAFS_NET
# variable "GANDI_SHARING_ID_TAHOE_LAFS_NET" {
#  type        = string
#  description = "Gandi.net sharing ID"
# }

provider "gandi" {
  key = var.GANDI_API_KEY_TAHOE_LAFS_NET
  # # sharing_id is for organizations; unused here.
  # sharing_id = var.GANDI_SHARING_ID_TAHOE_LAFS_NET
}

data "gandi_domain" "tahoe-lafs_net" {
  name = "tahoe-lafs.net"
}

# We'll get OSUOSL's help to host tahoe-lafs mailing lists, under the
# subdomain lists.tahoe-lafs.net.  For that to work, we're going to
# need some DNS records:
#
# lists.tahoe-lafs.net    A       140.211.9.53
#                         AAAA    2605:bc80:3010:104::8cd3:935
#                         TXT     "v=spf1 mx include:_spf.osuosl.org ~all"
#                         MX 5    smtp1.osuosl.org.
#                         MX 5    smtp2.osuosl.org.
#                         MX 5    smtp3.osuosl.org.
#                         MX 5    smtp4.osuosl.org.

resource "gandi_livedns_record" "lists-a" {
  zone = data.gandi_domain.tahoe-lafs_net.id
  name = "lists"
  type = "A"
  ttl  = 3600
  values = [
    "140.211.9.53"
  ]
}

resource "gandi_livedns_record" "lists-aaaa" {
  zone = data.gandi_domain.tahoe-lafs_net.id
  name = "lists"
  type = "AAAA"
  ttl  = 3600
  values = [
    "2605:bc80:3010:104::8cd3:935"
  ]
}

resource "gandi_livedns_record" "lists-txt" {
  zone = data.gandi_domain.tahoe-lafs_net.id
  name = "lists"
  type = "TXT"
  ttl  = 3600
  values = [
    "\"v=spf1 mx include:_spf.osuosl.org ~all\""
  ]
}

resource "gandi_livedns_record" "lists-mx" {
  zone = data.gandi_domain.tahoe-lafs_net.id
  name = "lists"
  type = "MX"
  ttl  = 3600
  values = [
    "5 smtp1.osuosl.org.",
    "5 smtp2.osuosl.org.",
    "5 smtp3.osuosl.org.",
    "5 smtp4.osuosl.org.",
  ]
}
