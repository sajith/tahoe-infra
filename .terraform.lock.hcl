# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "github/go-gandi/gandi" {
  version     = "2.0.0"
  constraints = "2.0.0"
  hashes = [
    "h1:G8ZjlDZAqqxeD01+b6mAUKxNLsKuazZC+dxG9E62thI=",
  ]
}

provider "registry.terraform.io/sajith/gandi" {
  version     = "2.0.0-rc3b"
  constraints = "2.0.0-rc3b"
  hashes = [
    "h1:jMTyg4Iw5oFK3EHRz2/t0qOgjKRA16g7duIhWbbMhkU=",
    "h1:m4pGClbCGwSz/LUeyOc6qJ7j3UhaShcqKXABftJ3eTI=",
    "zh:6bf76c12099b1e06f902427d8d78df42868cc57ac875fe33e1a3c0cf40c87b3b",
    "zh:8c13d10a0c8059600373ab60d88fe651bee2adea7f30a6ec6553ae4e58ef5cde",
  ]
}
